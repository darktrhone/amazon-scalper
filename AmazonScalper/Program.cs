﻿namespace AmazonScalper
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using PuppeteerSharp;

    public class Program
    {
        private const string path = "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe";

        private static Dictionary<string, Product> productsOnHighDemand;
        private static Dictionary<string, Product> products1;
        private static Dictionary<string, Product> products2;
        private static Dictionary<string, Product> products3;
        private static Dictionary<string, Product> products4;


        private static List<string> LinksOnHighDemand;
        private static List<string> Links1;
        private static List<string> Links2;
        private static List<string> Links3;
        private static List<string> Links4;

        static async Task Main()
        {
            productsOnHighDemand = new Dictionary<string, Product>();
            products1 = new Dictionary<string, Product>();
            products2 = new Dictionary<string, Product>();
            products3 = new Dictionary<string, Product>();
            products4 = new Dictionary<string, Product>();

            await LoadUrls();

            Console.WriteLine("How many bots?");
            var x = Convert.ToInt32(Console.ReadLine());

            if (x >= 2)
            {
                Task.Run(() => LaunchPuppet("1", products1, Links1));
                Thread.Sleep(15000);
            }

            if (x >= 3)
            {
                Task.Run(() => LaunchPuppet("2", products2, Links2));
                Thread.Sleep(15000);
            }

            if (x >= 4)
            {
                Task.Run(() => LaunchPuppet("3", products3, Links3));
                Thread.Sleep(15000);
            }
            if (x >= 5)
            {
                Task.Run(() => LaunchPuppet("4", products4, Links4));
                Thread.Sleep(15000);
            }
 
            await Task.Run(() => LaunchPuppet("HD", productsOnHighDemand, LinksOnHighDemand));
        }

        private static async Task LaunchPuppet(string id, Dictionary<string, Product> products, List<string> links)
        {
            await using var headlessBrowser = await Puppeteer.LaunchAsync(new LaunchOptions {ExecutablePath = path});
            await using var headlessPage = await headlessBrowser.NewPageAsync();

            Product product = null;
            int i = 0;
            while (true)
            {
                if (i == links.Count)
                    i = 0;

                product = GetProduct(links[i++], products);
                try
                {
                    if (IsCached(product))
                    {
                        continue;
                    }

                    await headlessPage.GoToAsync(product.Link);

                    if (await IsOutOfStockProduct(headlessPage))
                    {
                        var name = await GetOutOfStockProductName(headlessPage, product);

                        // if (name.Contains("portatil") || name.Contains("portátil") || name.Contains("Portatil") ||
                        //     name.Contains("Portátil"))
                        //     await File.AppendAllTextAsync("errors", fullUrl + "," + Environment.NewLine);
                        //
                        if (name.Contains("Block") || name.Contains("block"))
                            await File.AppendAllTextAsync("errors",
                                product.Link + ",  " + product.Name + Environment.NewLine);


                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write("WorkerID:" + id + " Out of Stock: ");
                        Console.ResetColor();
                        Console.WriteLine(name);
                        continue;
                    }

                    var productName = await GetStockedProductName(headlessPage, product);
                    var maxAllowed = GetMax(productName);

                    var productPrice = await GetProductPrice(headlessPage, product);

                    if (productPrice > 3000)
                        await File.AppendAllTextAsync("errors", product.Link + "," + Environment.NewLine);


                    if (productPrice <= maxAllowed)
                    {
                        Console.ForegroundColor = ConsoleColor.DarkGreen;
                        Console.WriteLine($"WorkerID:" + id + $" Match found: {productPrice}EUR {productName}");
                        Console.ResetColor();
                        Process.Start(path, product.Link);
                        product.Expiry = DateTimeOffset.UtcNow + TimeSpan.FromMinutes(5);
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.DarkYellow;
                        Console.Write($"WorkerID:" + id + " Product Overpriced:");
                        Console.WriteLine(product.Link);
                        Console.WriteLine($" Price: {productPrice}EUR MAX: {maxAllowed}EUR {productName} ");
                        Console.ResetColor();
                    }
                }
                catch (Exception e)
                {
                    await File.AppendAllTextAsync("errors", product.Link + "," + Environment.NewLine);
                }
            }
        }

        private static async Task LoadUrls()
        {
            var fileLinks = await File.ReadAllTextAsync("AmazonLinks.txt");
            var fileLinksDemand = await File.ReadAllTextAsync("AmazonLinksDemand.txt");
            var errors = await File.ReadAllTextAsync("errors");

            var links = fileLinks.Replace(Environment.NewLine, "")
                .Split(",", StringSplitOptions.RemoveEmptyEntries);

            var links2 = errors.Replace(Environment.NewLine, "")
                .Split(",", StringSplitOptions.RemoveEmptyEntries);

            var linksDemand = fileLinksDemand.Replace(Environment.NewLine, "")
                .Split(",", StringSplitOptions.RemoveEmptyEntries);
            
            links = links.Except(links2).Except(linksDemand).ToArray();

            Links1 = new List<string>(links);
            Links2 = new List<string>(links);
            Links3 = new List<string>(links);
            Links4 = new List<string>(links);
            LinksOnHighDemand = new List<string>(linksDemand);

            products1 = new Dictionary<string, Product>();
            products2 = new Dictionary<string, Product>();
            products3 = new Dictionary<string, Product>();
            products4 = new Dictionary<string, Product>();

            foreach (var hdLink in linksDemand)
            {
                productsOnHighDemand.TryAdd(hdLink, new Product());
            }
            
            foreach (var link in links)
            {
                products1.TryAdd(link, new Product());
                products2.TryAdd(link, new Product());
                products3.TryAdd(link, new Product());
                products4.TryAdd(link, new Product());
            }
        }

        private static bool IsCached(Product product)
        {
            return product.Expiry >= DateTimeOffset.UtcNow;
        }

        private static async Task<bool> IsOutOfStockProduct(Page page)
        {
            return await page.QuerySelectorAsync(".item-row a img") == null;
        }

        private static Product GetProduct(string url, Dictionary<string, Product> products)
        {
            var product = products.FirstOrDefault(x => x.Key == url).Value;

            product.Link = url;

            return product;
        }


        private static int GetMax(string text)
        {
            if (text.Contains("3060TI") || text.Contains("3060 TI"))
                return 700;
            if (text.Contains("3060"))
                return 600;
            if (text.Contains("3070") || text.Contains("6800 XT") || text.Contains("6800XT"))
                return 800;
            if (text.Contains("3080"))
                return 1000;

            return 111110;
        }


        private static async Task<decimal> GetProductPrice(Page page, Product product)
        {
            var productPrice = await page.QuerySelectorAsync(".price.item-row")
                .EvaluateFunctionAsync<string>("e => e.innerText");

            var price = decimal.Parse(productPrice.Replace(".", "").Substring(0, productPrice.IndexOf(',')));

            product.Price = price;

            return product.Price;
        }

        private static async Task<string> GetStockedProductName(Page page, Product product)
        {
            if (string.IsNullOrEmpty(product.Name))
            {
                var row = await page.QuerySelectorAllAsync(".item-row");

                var name = await row[1].QuerySelectorAsync("a")
                    .EvaluateFunctionAsync<string>("e => e.innerText");

                product.Name = name;
            }

            return product.Name;
        }

        private static async Task<string> GetOutOfStockProductName(Page page, Product product)
        {
            if (string.IsNullOrEmpty(product.Name))
            {
                var row = await page.QuerySelectorAllAsync(".itemq");

                var productRow = await row[0].QuerySelectorAllAsync("td");

                var name = await productRow[1].EvaluateFunctionAsync<string>("e => e.innerText");

                product.Name = name;
            }

            return product.Name;
            //return name.Length <= 120 ? name : name.Substring(0, 120) + "...";
        }
    }
}