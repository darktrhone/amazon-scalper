﻿namespace AmazonScalper
{
    using System;

    public class Product
    {
        public string Name { get; set; }
        
        public decimal Price { get; set; }
        
        public string Link { get; set; }

        public DateTimeOffset Expiry { get; set; }


    }
}